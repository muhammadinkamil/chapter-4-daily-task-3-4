module.exports = {
  // filter data based on fruit banana
  filterData : function (datas, attribute) {
    let filtered_data = [];
    filtered_data = datas.filter((data)=>{
        return data.favoriteFruit == attribute.favoriteFruit || data.age == attribute.age || data.eyeColor == attribute.eyeColor || data.company == attribute.company
    })
    return filtered_data
  },
  // Accepts the array and key
groupBy : (array, key) => {
  // Return the end result
  return array.reduce((result, currentValue) => {
    // If an array already present for key, push it to the array. Else create an array and push the object
    (result[currentValue[key]] = result[currentValue[key]] || []).push(currentValue);
    // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
    return result;
  }, {}); // empty object is the initial value for result object
}
}