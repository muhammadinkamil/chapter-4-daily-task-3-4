// //panggil core module http
// let http = require("http");
// //panggil core module filesystem
const fs = require("fs");
//panggil core module path
const path = require("path");
//panggil module express
const express = require("express");
//assign module express ke var expr
const expr = express();
const serveStat = require("serve-static");
//assign file json berisikan data random dari json generator
let rawData = fs.readFileSync("datas.json");
//assign data dalam bentuk array object
const datas = require("./datas.js");
//assign data yang telah diparsing data JSON yang diterima
const parsing = JSON.parse(rawData);
//destructuring object menjadi var dari sortData.js
const { filterData, groupBy } = require("./filterData.js");
//assign data parsing data JSON yang hendak dikirim
const data = JSON.stringify(datas);
//panggil modul dotenv
require("dotenv").config();
//destructuring object menjadi var dari .env
const { PORT = 8080 } = process.env;
//menampung data yang telah d grup
let eyeColor = groupBy(parsing, "eyeColor");
let company = groupBy(parsing,"company");
let favoriteFruit = groupBy(parsing,"favoriteFruit");


//deklarasi function untuk render file html
function getHTML(htmlFileName) {
  const htmlFilePath = path.join(dir, htmlFileName);
  return fs.readFileSync(htmlFilePath, "utf-8");
}

//assign directory path untuk get html
const dir = path.join(__dirname, "resources/views");
expr.use(serveStat(dir));
const nodemodules = path.join(__dirname, "node_modules");
expr.use(serveStat(nodemodules));
const css = path.join(__dirname, "resources/css");
expr.use(serveStat(css));
const img = path.join(__dirname, "resources/img");
expr.use(serveStat(img));
const script = path.join(__dirname, "resources/script");
expr.use(serveStat(script));


// set the view engine to ejs
expr.set('view engine', 'ejs');
expr.set('views', path.join(__dirname, '/resources/views'));
expr.get("/",(req, res) => {
  res.send(eyeColor)
  res.render("homepage",{eyeColor,company,favoriteFruit});
});
expr.get("/data", (req, res) => {
  let datas = filterData(parsing,req.query);
  //console.log(datas)
  //console.log(req.query);
  res.render("homepage",{datas,eyeColor,company,favoriteFruit});
});
expr.get("/about", (req, res) => {
  res.render("about");
});
expr.use(function(req, res, next) {
  res.status(404).send(getHTML("404.html"));
});
expr.listen(PORT, function () {
  console.log("Example app listening on port 3000.");
});